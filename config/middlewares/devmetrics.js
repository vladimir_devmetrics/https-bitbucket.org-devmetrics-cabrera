/* globals require */
(function() {
  'use strict';

  module.exports = function(app, config, options) {
    var token = options['token'];
    var host = options['host'];


    ///// LOGS
    var winston = require('winston'); require('winston-logstash');
    winston.emitErrs = true;
    var loggerObj = new winston.Logger({
        transports: [
            new winston.transports.Logstash({
                level: 'info',
                port: 5545,
                node_name: token,
                host: host,
                handleExceptions: true
            }),
            new winston.transports.Console({
                level: 'debug',
                handleExceptions: true,
                json: false,
                colorize: true,
            })
        ],
        exitOnError: false
    });

    loggerObj.info('devmetrics logger init');


    ///// STATSD SENDER
    var lynx = require('lynx');
    var metrics = new lynx(host, 5546, {scope:token});

    loggerObj.info('devmetrics metrics init');


    ///// REQUEST LOGS
    var logsStream  = {
        write: function(message, encoding){
            loggerObj.info(message);
        }
    };

    var morgan = require('morgan');
    morgan.token = morgan.token('statsdKey', function getStatsdKey(req) {
        return req.statsdKey
        })

    var requestLogHandler = morgan('{"statsdKey":":statsdKey", "message":":method :url :status :res[content-length] - :response-time ms"}', { 'stream': logsStream })

    loggerObj.info('devmetrics request log init');

    var statsdURL = function (req, res, next) {
        var method = req.method || 'unknown_method';

        req.statsdKey = ['http', req.path.replace(/\//g, "-")].join('-');
        next();
      };


    ///// REQUEST METRICS
    var expressStatsd = require('express-statsd');
    var requestMetricHandler = expressStatsd({'client': metrics})

    loggerObj.info('devmetrics request metrics init');

    app.use(statsdURL);
    app.use(requestLogHandler);
    app.use(requestMetricHandler);

    // var express = require('express-statsd');
    // app.use(express.logger({stream: logStream}));


    // var mongoose = require('mongoose')
    // mongoose.set('debug', function(collectionName, method, query, doc) {
      // loggerObj.log('db_query: ' + collectionName + ' ' + method + ' ' + query + ' ' + doc);
    // });
  }



})();
